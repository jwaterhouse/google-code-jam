import os

n = int(input())
for ni in range(1, n + 1):
	l = input().split()
	length = len(l)
	for i in range(0, int(length / 2)):
		temp = str(l[i])
		l[i] = str(l[length - 1 - i])
		l[length - 1 - i] = temp
		
	s = ""
	for li in l: s += " " + li
	s = s.strip()
	print("Case #%d: %s" % (ni, s))
