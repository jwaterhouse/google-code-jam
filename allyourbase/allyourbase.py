import os

t = int(input())
for ti in range(1, t+1):
	s = list(input())
	
	
	#base = number of unique characters
	digits = dict()
	value = 1;
	i = 0
	for si in s:
		if digits.get(si) == None:
			if (i!= 1):
				digits[si] = value
				value += 1
			else:
				digits[si] = 0
			
			i += 1
		
	base = 2 if len(digits) <= 1 else len(digits)
	
	# compute the number
	seconds = 0
	i = len(s) - 1
	for si in s:
		seconds += digits[si] * base ** i
		i -= 1
		
	print("Case #%d: %d" % (ti, seconds))
