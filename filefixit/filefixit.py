import os

class directory:
	def __init__(self, name):
		self.name = name
		self.children = dict()
	def hasChild(name):
		return not c[name] == None
	def addChild(self, name):
		if not self.hasChild(name):
			children.append(directory(name))
		return children[len(children) - 1]
	def createSubDirectory(self, path):
		if (len(path) == 0):
			return 0
		try:
			child = self.children[path[0]]
			del path[0]
			return child.createSubDirectory(path)
		except KeyError:
			self.children[path[0]] = directory(path[0])
			child = self.children[path[0]]
			del path[0]
			return 1 + child.createSubDirectory(path)

t = int(input())
for ti in range(1, t + 1):
	
	s = input().split()
	n = int(s[0]) 
	m = int(s[1])
	root = directory("/")
	for ni in range(0, n):
		sn = input().split("/")
		del sn[0]
		root.createSubDirectory(sn)
	count = 0
	for mi in range(0, m):
		sm = input().split("/")
		del sm[0]
		count += root.createSubDirectory(sm)
	print("Case #%d: %d" % (ti, count))
