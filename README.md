My solutions to some of the Google Code Jam problems.

Most are written in Python3 but some are in C++.

Since input is taken from standard in and output is written to standard out, you can invoke them with something like "python code.py < input.in > output.txt".