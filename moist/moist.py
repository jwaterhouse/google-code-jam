import os

t = int(input())
for i in range(1, t + 1):
	count = 0
	n = int(input())
	cards = []
	for ni in range(0, n):
		cards.append(input())
		
	# sort the cards
	highestCard = cards[0]
	for c in range(1, len(cards)):
		if cards[c] < highestCard:
			count += 1
		else:
			highestCard = cards[c]
	
	print("Case #%d: %d" % (i, count))
