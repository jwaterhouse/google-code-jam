import os

class Letter:
	def __init__(self, letter):
		self.letter = letter
		self.children = dict()
	def hasChild(letter):
		return not c.get(letter) == None
	def addChild(self, letter):
		if not self.hasChild(letter):
			children.append(Letter(letter))
		return children[len(children) - 1]
	def createSub(self, word):
		if (len(word) == 0):
			return 0
		try:
			child = self.children[word[0]]
			del word[0]
			return child.createSub(word)
		except KeyError:
			self.children[word[0]] = Letter(word[0])
			child = self.children[word[0]]
			del word[0]
			return 1 + child.createSub(word)
	def wordExists(self, word, i):
		if i >= len(word):
			return True
		if not self.children.get(word[i]) == None:
			return self.children[word[i]].wordExists(word, i + 1)
		else:
			return False

def traverse(node, d, i):
	if i == len(d):
		return 1
	count = 0
	for di in d[i]:
		child = node.children.get(di)
		if child != None:
			count += traverse(child, d, i + 1)
	return count
			

def parseInput(s):
	ss = list(s)
	d = []
	i = 0
	parMode = False
	for si in ss:
		if si == '(':
			parMode = True
			d.append([])
			continue
		if si == ')':
			parMode = False
			continue
		if parMode:
			d[len(d) - 1].append(si)
		else:
			d.append([si])
	return d

s = input().split()
l = int(s[0])
d = int(s[1])
n = int(s[2])
root = Letter("/")
for di in range(0, d):
	word = list(input())
	root.createSub(word)
for ni in range(1, n + 1):
	word = parseInput(input())
	k = traverse(root, word, 0)
	print("Case #%d: %d" % (ni, k))
