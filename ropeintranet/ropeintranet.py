import os

t = int(input())

for ti in range(1, t + 1):
	n = int(input())
	wires = []
	for ni in range(0, n):
		s = input().split()
		wires.append([int(s[0]), int(s[1])])
	count = 0
	for i in range(0, len(wires)):
		for j in range(i + 1, len(wires)):
			if ((wires[i][0] > wires[j][0]
				and wires[i][1] < wires[j][1]) or
				(wires[i][0] < wires[j][0]
				and wires[i][1] > wires[j][1])):
					count += 1
	print("Case #%d: %d" % (ti, count))
