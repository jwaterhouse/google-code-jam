#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <cstdlib>
#include "StringTrim.h"

class Henchman
{
public:
    std::string name;
    std::vector<std::string> conflicts;
    Henchman(const std::string& name, const std::string& conflict)
        : name(name), conflicts()
    {
        //this->name = std::string(name);
        this->conflicts.push_back(std::string(conflict.c_str(), conflict.length()));
    }
    virtual ~Henchman() {}
    void addConflict(std::string conflict)
    {
        for (std::vector<std::string>::iterator it = conflicts.begin(); it != conflicts.end(); ++it)
        {
            if(*it == conflict)
                return;
        }
        conflicts.push_back(conflict);
    }
    bool hasConflict(const Henchman& henchman)
    {
        for(unsigned int i = 0; i < conflicts.size(); ++i)
        {
            if(conflicts.at(i) == henchman.name)
                return true;
        }
        return false;
    }
};

void addHenchman(std::vector<Henchman*>& henchmen, const std::string &hString)
{
    int index = hString.find(' ');
    std::string name = hString.substr(0, index);
    std::string conflict = hString.substr(index + 1);
    bool exists = false;
    for(std::vector<Henchman*>::iterator it = henchmen.begin(); it != henchmen.end(); ++it)
    {
        if((*it)->name == name)
        {
            exists = true;
            (*it)->addConflict(conflict);
        }
    }
    if(!exists)
        henchmen.push_back(new Henchman(name, conflict));

    exists = false;
    for(std::vector<Henchman*>::iterator it = henchmen.begin(); it != henchmen.end(); ++it)
    {
        if((*it)->name == conflict)
        {
            exists = true;
            (*it)->addConflict(name);
        }
    }
    if(!exists)
        henchmen.push_back(new Henchman(conflict, name));
}

bool teamOkay(std::vector<Henchman*> henchmen)
{
    for (std::vector<Henchman*>::iterator it1 = henchmen.begin(); it1 != henchmen.end(); ++it1)
    {
        for (std::vector<Henchman*>::iterator it2 = it1 + 1; it2 != henchmen.end(); ++it2)
        {
            if ((*it1)->hasConflict(**it2))
                return false;
        }
    }
    return true;
}

bool checkTeams(std::vector<Henchman*> t1, std::vector<Henchman*> t2, int n)
{
    bool p1 = teamOkay(t1);
    if (!p1)
        return false;

    bool p2 = teamOkay(t2);

    if (p1 && p2)
        return true;
    if (t2.size() > 1)
    {
        for(int i = 0; i < t2.size(); ++i)
        {
            std::vector<Henchman*> nt1(t1);
            std::vector<Henchman*> nt2(t2);
            nt1.push_back(nt2.at(i));
            nt2.erase(nt2.begin() + i);

            if (t1.size() <= t2.size() ||
                (t1.size() > t2.size() && i >= n))
            {
                if (checkTeams(nt1, nt2, i))
                    return true;
            }
        }
    }

    return false;
}

std::vector<unsigned int> getConflicts(std::vector<Henchman*>& henchmen)
{
    std::vector<unsigned int> conflictIndices;
    for(unsigned int i = 0; i < henchmen.size(); ++i)
    {
        for(unsigned int j = 0; j < henchmen.size(); ++j)
        {
            if(i == j)
                continue;
            if(henchmen.at(i)->hasConflict(*(henchmen.at(j))))
            {
                conflictIndices.push_back(i);
                break;
            }
        }
    }
    return std::vector<unsigned int>(conflictIndices);
}

bool checkTeams2(std::vector<Henchman*>& t1, std::vector<Henchman*>& t2, int n, int& checked)
{
    checked++;
    //std::cout << "Checking: " << checked << std::endl;
    std::vector<unsigned int> c1 = getConflicts(t1);
    if (c1.size() != 0)
    {
        //std::cerr << "Trim: " << t1.size() << " : " << t2.size() << std::endl;
        return false;
    }

    std::vector<unsigned int> c2 = getConflicts(t2);

    if (c2.size() == 0)
    {
        return true;
    }

    if (t2.size() < 1)
        return false;

    for (int i = 0; i < c2.size(); ++i)
    {
        std::vector<Henchman*> nt1(t1);

        nt1.push_back(t2.at(c2.at(i)));
        std::vector<Henchman*> nt2(t2.begin(), t2.begin() + c2.at(i));
        nt2.insert(nt2.end(), t2.begin() + c2.at(i) + 1, t2.end());

        if (t1.size() <= t2.size() || c2[i] >= n)
        {
            if (checkTeams2(nt1, nt2, c2[i], checked))
                return true;
        }
    }

    return false;
}

int main(int argc, char* argv[])
{
    //std::ifstream infile("sample2.in");
    //std::ifstream infile("A-small-practice-2.in");
    std::string line = "";
    std::getline(std::cin, line);
    line = trim(line);
    int t = std::atoi(line.c_str());
    //std::cout << t << std::endl;
    for(int ti = 1; ti < t + 1; ++ti)
    {
        std::getline(std::cin, line);
        line = trim(line);
        int m = std::atoi(line.c_str());
        //std::cout << m << std::endl;
        std::vector<Henchman*> henchmen;
        for(int mi = 0; mi < m; ++mi)
        {
            std::getline(std::cin, line);
            line = trim(line);
            //std::cout << line << std::endl;
            addHenchman(henchmen, line);
        }
        std::vector<Henchman*> t1;
        t1.push_back(henchmen.at(0));
        std::vector<Henchman*> t2(henchmen);
        t2.erase(t2.begin());


        std::cout << henchmen.size() << std::endl;
        int checked = 0;
        //bool okay = checkTeams(t1, t2, 0);
        bool okay2 = checkTeams2(t1, t2, 0, checked);
        std::cout << "Checked: " << checked << std::endl;
        std::cout << "Case #" << ti << ": " << (okay2 ? "Yes" : "No") << std::endl;
        for (std::vector<Henchman*>::iterator it = henchmen.begin(); it != henchmen.end(); ++it)
        {
            delete *it;
            *it = 0;
        }
    }
}
