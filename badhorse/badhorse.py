import os
import sys

class Henchman:
	def __init__(self, name, conflict):
		self.name = name;
		self.conflicts = []
		self.conflicts.append(conflict)
	def addConflict(self, conflict):
		for c in self.conflicts:
			if c == conflict:
				return
		self.conflicts.append(conflict)
	def hasConflict(self, henchman):
		for c in self.conflicts:
			if c == henchman.name:
				return True
		return False
	def __str__(self):
		return str(self.name, self.conflicts)
	def __unicode__(self):
		return str(self.name, self.conflicts)
	def __repr__(self):
		return str((self.name, self.conflicts))

class Henchmen:
	def __init__(self):
		self.henchmen = []
	def add(self, s):
		tok = s.split()
		name1 = tok[0]
		name2 = tok[1]
		h1 = filter(lambda h: h.name == name1, self.henchmen)
		try:
			next(h1).addConflict(name2)
		except StopIteration:
			self.henchmen.append(Henchman(name1, name2))
			
		h2 = filter(lambda h: h.name == name2, self.henchmen)
		try:
			next(h2).addConflict(name1)
		except StopIteration:
			self.henchmen.append(Henchman(name2, name1))
	def indexOf(self, name):
		for i in range(0, self.henchmen):
			if(self.henchmen[i].name == name):
				return i
		return -1

def HashTeam(team):
	string = ""
	for t in team:
		string += t.name
	return hash(string)
	
def TeamOkay(team):
	for i in range(0, len(team)):
		for j in range(i + 1, len(team)):
			if team[i].hasConflict(team[j]):
				return False
	return True
	

def CheckTeams(t1, t2, n):
	#print(n, len(team1), len(team2))
	
	possible1 = TeamOkay(t1)
	if not possible1:
		return False
		
	possible2 = TeamOkay(t2)
	if possible2:
		return True

	if len(t2) > 1:
		for i in range(0, len(t2)):
			nt1 = list(t1)
			nt1.append(t2[i])
			nt1 = sorted(nt1, key=lambda h:  h.name)
			nt2 = list(t2[0:i] + t2[i + 1:len(t2)])
			if (len(t1) <= len(t2)) or ((len(t1) > len(t2)) and (i >= n)):
				possible3 = CheckTeams(nt1, nt2, i)
				if possible3:
					return possible3
	return False
	
def GetConflicts(t):
	conflicts = []
	for i in range(0, len(t)):
		for j in range(0, len(t)):
			if i == j:
				continue
			if t[i].hasConflict(t[j]):
				conflicts.append(i)
				break
	return conflicts
	
def CheckTeams2(t1, t2):
	c1 = GetConflicts(t1)
	if(len(c1) > 0):
		return False
	
	c2 = GetConflicts(t2)
	
	if(len(c2) == 0):
		return True
	
	if(len(t2) <= 1):
		return False
	
	for i in range(0, len(c2)):
		nt1 = list(t1)
		nt1.append(t2[c2[i]])
		nt2 = t2[0:c2[i]] + t2[c2[i]+1:len(t2)]
		if (CheckTeams2(nt1, nt2)):
			return True
	return False
	
def CheckEdges(t1, t2, edges, n):
	#print(t1, t2)
	if len(t2) == 0:
		return False
		
	for i in t1:
		for j in t1:
			if i == j: continue
			if edges[i][j] == True:
				return False
	
	okay = True
	for i in range(0, len(t2)):
		#if i < n:
		#	continue
		for j in range(0, len(t2)):
			if i == j: continue
			if edges[t2[i]][t2[j]] == True:
				okay = False
				nt1 = list(t1)
				nt1.append(t2[i])
				nt2 = t2[0:i] + t2[i + 1:len(t2)]
				if (CheckEdges(nt1, nt2, edges, i)):
					return True
				else:
					break
					
	return okay

t = int(input())
for i in range(1, t + 1):
	m = int(input())
	h = Henchmen()
	for j in  range(0, m):
		s = input().strip()
		h.add(s)
	# now iterate through each team combination to see if it works
	h.henchmen = sorted(h.henchmen, key=lambda h:  h.name)
	
	edges = []
	for hi in range(0, len(h.henchmen)):
		edges.append(list());
		for hj in range(0, len(h.henchmen)):
			if (h.henchmen[hi].hasConflict(h.henchmen[hj])):
				edges[hi].append(True)
			else:
				edges[hi].append(False)
	
	#possible = "Yes" if CheckTeams(h.henchmen[0:1],
	#	h.henchmen[1:len(h.henchmen)], 0) else "No"
	#possible = "Yes" if CheckTeams2(h.henchmen[0:1],
	#	h.henchmen[1:len(h.henchmen)]) else "No"
	
	#print(edges)
	
	t1 = [0]
	t2 = [j for j in range(1, len(h.henchmen))]
	#print(t1, t2)
	possible = "Yes" if CheckEdges(t1, t2, edges, 0) else "No"
	
	print("Case #" + str(i) + ": " + possible)
