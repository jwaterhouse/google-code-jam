import os

def checkBoard(board, x, y, k, c):
	# check row
	yi = y
	count = 0
	while yi < len(board[x]) and board[x][yi] == c:
		count += 1
		yi += 1
	yi = y - 1
	while yi >= 0 and board[x][yi] == c:
		count += 1
		yi -= 1
	if count >= k:
		return True
		
	# check column
	xi = x
	count = 0
	while xi < len(board) and board[xi][y] == c:
		count += 1
		xi += 1
	xi = x - 1
	while xi >= 0 and board[xi][y] == c:
		count += 1
		xi -= 1
	if count >= k:
		return True
		
	# check diagonal 1
	xi = x
	yi = y
	count = 0
	while  xi < len(board) and yi < len(board[xi]) and board[xi][yi] == c:
		count += 1
		xi += 1
		yi += 1
	xi = x - 1
	yi = y - 1
	while xi >= 0 and yi >= 0 and board[xi][yi] == c:
		count += 1
		xi -= 1
		yi -= 1
	if count >= k:
		return True
	
	# check diagonal 2
	xi = x
	yi = y
	count = 0
	while xi < len(board) and yi >= 0 and board[xi][yi] == c:
		count += 1
		xi += 1
		yi -= 1
	xi = x - 1
	yi = y + 1
	while xi >= 0 and yi < len(board[xi]) and board[xi][yi] == c:
		count += 1
		xi -= 1
		yi += 1
	if count >= k:
		return True
	return False

t = int(input())
for ti in range(1, t+1):
	s = input().split()
	n = int(s[0])
	k = int(s[1])
	board = []
	for ni in range(0, n):
		board.append([])
		s = list(input())
		for si in s:
			board[ni].append(si)
			
	for b in board:
		spaces = 0
		for bi in range(len(b)-1, -1, -1):
			if b[bi] == ".":
				spaces += 1
			elif spaces > 0:
				b[bi + spaces] = b[bi]
				b[bi] = "."
	
	r = False
	b = False
	for x in range(0, len(board)):
		if r and b:
			break
		for y in range(len(board[x])-1, -1, -1):
			#filter out illegal positions
			if y < len(board[x]) - 1 and board[x][y+1] == ".":
				continue;
			if board[x][y] == ".":
				if not r and checkBoard(board, x, y, k, 'R'):
					r = True
				if not b and checkBoard(board, x, y, k, 'B'):
					b = True
				break
			elif board[x][y] != ".":
				check = checkBoard(board, x, y, k, board[x][y])
				if board[x][y] == 'B' and check:
					b = True
				if board[x][y] == 'R' and check:
					r = True
	result = "Both" if r and b else "Red" if r else "Blue" if b else "Neither"
	print("Case #%d: %s" % (ti, result))
		
