import os

n = int(input())
for ni in range(1, n + 1):
	c = int(input())
	i = int(input()) 
	p = (input().split())
	for pi in range(0, len(p)): p[pi] = int(p[pi])
	
	found = False
	for pi in range(0, len(p)):
		if p[pi] >= c: continue
		for pj in range(pi + 1, len(p)):
			if p[pi] + p[pj] == c:
				print("Case #%d: %d %d" % (ni, pi + 1, pj + 1))
				found = True
				break
		if found: break

