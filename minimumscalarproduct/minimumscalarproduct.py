import os

t = int(input())
for ti in range(1, t + 1):
	n = int(input())
	v1s = input().split()
	v1 = []
	for v in v1s:
		v1.append(int(v))
	v2s = input().split()
	v2 = []
	for v in v2s:
		v2.append(int(v))
	v1 = sorted(v1, reverse=True)
	v2 = sorted(v2)
	product = 0
	for i in range(0, len(v1)):
		product += v1[i] * v2[i]
	print("Case #%d: %d" % (ti, product))
