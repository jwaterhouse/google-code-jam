import os;
import math;

def CalculateD(v, a, theta):
	vVert = v * math.sin(theta)
	t = 2.0 * -vVert / a
	vHor = v * math.cos(theta)
	return vHor * t


a = -9.8
t = int(input())
for i in range(1, t + 1):
	s = input().strip().split()
	v = float(s[0])
	d = float(s[1])
	theta = math.radians(45.0 / 2.0)
	
	inc = theta
	dCalc = CalculateD(v, a, theta)
	j = 0;
	while (math.fabs(1.0 - dCalc / d) > 10**-14):
		inc /= 2.0
		if dCalc > d:
			theta = theta - inc
		else:
			theta = theta + inc
		dCalc = CalculateD(v, a, theta)
		j += 1
	print("Case #%d: %.7f" % (i, math.degrees(theta)))
